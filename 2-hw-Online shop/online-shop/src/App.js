import React, {Component} from "react";
import Modal from "./components/Modal";
import Header from "./components/Header";
import Item from "./components/Item";
import ItemList from "./components/ItemWrapper";
import getData from "./itemRequest/getData";
import './App.scss';

class App extends Component {
    state = {
        isModal: false,
        items: [],
    }

    componentDidMount() {
        getData()
            .then((data) => {
                this.setState((prevState) => {
                    return {
                        ...prevState,
                        items: data
                    }
                })
            })
    }

    handlerToggleModal = () => {
        this.setState((prevState) => {
            return {
                ...prevState,
                isModal: !prevState.isModal
            }
        })
    }

    handlerCurrentItem = (currentItem) => {
        localStorage.setItem("currentItem", JSON.stringify(currentItem));
    }

    handlerAddItem = () => {
        const cart = JSON.parse(localStorage.getItem("cart"));
        const currentItem = JSON.parse(localStorage.getItem("currentItem"));

        localStorage.setItem("cart", JSON.stringify([...cart, currentItem]));
        this.setState((prevState) => {
            return {
                ...prevState,
                isModal: !prevState.isModal,
            }
        })
        // localStorage.removeItem("cart")
    }

    handleAddToFavoriteList = () => {
        const currentItem = JSON.parse(localStorage.getItem("currentItem"));
        const favorites =  JSON.parse(localStorage.getItem("favorites"));
        localStorage.setItem("favorites", JSON.stringify([...favorites, currentItem]));
        this.forceUpdate();
    }

    handleRemoveFromFavoriteList = () => {
        const currentItem = JSON.parse(localStorage.getItem("currentItem"));
        const favorites =  JSON.parse(localStorage.getItem("favorites"));
        const indexOfObj = favorites.findIndex((obj) => {
            return obj.article === currentItem.article;
        });

        const newFavorites = [...favorites.slice(0, indexOfObj), ...favorites.slice(indexOfObj+1, favorites.length)];
        localStorage.setItem("favorites", JSON.stringify(newFavorites));
        this.forceUpdate();
    }

    render() {
        if (JSON.parse(localStorage.getItem("favorites")) === null){
            localStorage.setItem("favorites" ,JSON.stringify([]))
        } else if (JSON.parse(localStorage.getItem("cart")) === null){
            localStorage.setItem("cart" ,JSON.stringify([]))
        }

        const {isModal,items} = this.state;
        const favorites =  JSON.parse(localStorage.getItem("favorites"));
        const cart = JSON.parse(localStorage.getItem("cart"));

        return (
            <>
                <Header cart={cart.length} favorites={favorites.length}/>
                <ItemList>
                    {items.map((item, id) => <Item
                        key={id}
                        data={item}
                        favorites={favorites}
                        handlerCurrentItem={this.handlerCurrentItem}
                        addHandler={this.handlerToggleModal}
                        handleAddToFavoriteList={() => {this.handleAddToFavoriteList()}}
                        handleRemoveFromFavoriteList={() => {this.handleRemoveFromFavoriteList()}}
                    />)}
                </ItemList>
                {isModal && <Modal
                    text="Add to shopping cart?"
                    handlerAddItem={() => this.handlerAddItem()}
                    closeModal={this.handlerToggleModal}
                />}
            </>
        )
    }
}

export default App;