import {Component} from 'react';
import PropTypes from 'prop-types';
import {ReactComponent as CartSvg} from "./icons/cart.svg";
import {ReactComponent as FavoriteSvg} from "./icons/favorites.svg";
import './Header.scss';

class Header extends Component {
	render() {
		const {cart, favorites} = this.props;
		return (
			<header className="header">
				<div className="container">
					<div className="header__wrapper">
						<div className="header__logo">
							<img src="https://content.rozetka.com.ua/goods/images/original/308326671.jpg" alt="Music-Logo" className="logo"/>
							<p className="logo-text">Life of musiC</p>
						</div>

						<div className="header__actions">
							<div className="header__favorites-list">
								<span className="icon-favorite">
									<span className="count">{favorites}</span>
									<FavoriteSvg/>
								</span>
							</div>
							<div className="header__cart-list">
								<span className="icon-cart">
									<span className="count">{cart}</span>
									<CartSvg />
								</span>
							</div>
						</div>
					</div>
				</div>
			</header>
		)
	}
}

Header.propTypes = {
	cart: PropTypes.number,
	favorites: PropTypes.number,
}

Header.defaultProps = {
	cart: 0,
	favorites: 0,
}

export default Header;
