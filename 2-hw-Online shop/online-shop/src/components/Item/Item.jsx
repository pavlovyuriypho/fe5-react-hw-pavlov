import {Component} from "react";
import PropTypes from 'prop-types';
import {ReactComponent as FavoriteSvg} from "./icons/favorite.svg";
import {ReactComponent as UnFavoriteSvg} from "./icons/unFavorite.svg";
import './Item.scss';
import Button from "../Button";

class Item extends Component{

    state = {
        isFavorite: this.props.favorites.some((item) => item.article === this.props.data.article),
    }

    handleFavoriteIcon = () => {
        const {handleAddToFavoriteList, handleRemoveFromFavoriteList} = this.props;
        const {isFavorite} = this.state;

        this.setState((prevState) => {
            return{
                ...prevState,
                isFavorite: !prevState.isFavorite,
            }
        })

        isFavorite ? handleRemoveFromFavoriteList() : handleAddToFavoriteList();

    }

    render() {
        const {name, price, img, article, color} = this.props.data;
        const {addHandler, handlerCurrentItem} = this.props
        const {isFavorite} = this.state;


        return(
            <div className="item">
                <div className="item__logo-container">
                    <img src={img} alt={name} className="item__image" />
                </div>
                <div className="item__content-container">
                    <h3 className="item__name">{name}</h3>
                    <p className="item__article">Article: {article}</p>
                    <p className="item__color">Color: {color}</p>
                    <p className="item__price">Price: <span className="item__price--big">{price}</span></p>
                </div>
                <div className="item__buttons-container">
                    <Button  className="item__add-btn" onClickHandler={() => {
                        handlerCurrentItem(this.props.data);
                        addHandler();
                    }}>ADD TO CART</Button>

                    <Button  className="item__favorite-btn" onClickHandler={() => {
                        handlerCurrentItem(this.props.data);
                        this.handleFavoriteIcon();
                    }}>
                        {isFavorite ? <FavoriteSvg/> : <UnFavoriteSvg/>}
                    </Button>
                </div>

            </div>
        )
    }
}

Item.propTypes = {
    handleAddToFavoriteList: PropTypes.func,
    handleRemoveFromFavoriteList: PropTypes.func,
    addHandler: PropTypes.func,
    currentItemHandler: PropTypes.func,
    data: PropTypes.object,
}

export default Item;