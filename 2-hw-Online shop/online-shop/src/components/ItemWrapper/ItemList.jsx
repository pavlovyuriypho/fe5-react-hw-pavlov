import {Component} from "react";
import './ItemList.scss';

class ItemList extends Component{
    render() {
        return(
            <div className="container">
                <div className="item-wrapper">
                    {this.props.children}
                </div>
            </div>
        )
    }
}

export default ItemList;
