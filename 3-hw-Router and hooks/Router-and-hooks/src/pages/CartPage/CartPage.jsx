import PageWrapper from "../../components/PageWrapper";
import ItemList from "../../components/ItemList";
import {useState} from "react";

function CartPage(){
    const cart = JSON.parse(localStorage.getItem("cart"));
    const [rerender, setRerender] = useState(false);

    return(
        <PageWrapper>
            <ItemList items={cart} setRerender={setRerender} rerender={rerender} isFavSvg={false} className="item-wrapper--cart"  isCartBtn={false}/>
        </PageWrapper>
    )
}

export default CartPage;
