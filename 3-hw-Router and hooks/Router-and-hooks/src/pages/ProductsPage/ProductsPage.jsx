import ItemList from "../../components/ItemList";
import {useEffect, useState} from "react";
import getData from "../../itemRequest/getData";
import PageWrapper from "../../components/PageWrapper";


function ProductsPage() {
    const [items, setItems] = useState([]);
    const [rerender, setRerender] = useState(false);

    useEffect(() => {
        getData()
            .then((data) => {
                setItems(data);
            })

    }, [])

    const favorites = JSON.parse(localStorage.getItem("favorites"));
    const cart = JSON.parse(localStorage.getItem("cart"));

    if (favorites === null) {
        localStorage.setItem("favorites", JSON.stringify([]));
    } else if (cart === null) {
        localStorage.setItem("cart", JSON.stringify([]));
    }

    return (
        <PageWrapper>
            <ItemList items={items} setRerender={setRerender} rerender={rerender}/>
        </PageWrapper>

    )
}

export default ProductsPage;
