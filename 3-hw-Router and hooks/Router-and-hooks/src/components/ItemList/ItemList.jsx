import './ItemList.scss';
import Item from "../Item";


function ItemList({items, setRerender, rerender, isFavSvg, className, isCartBtn}){

    return(
        <div className="container">
            <div className={className}>
                {items.map((item, id) => <Item
                    key={id}
                    data={item}
                    setRerender={setRerender}
                    rerender={rerender}
                    isFavSvg={isFavSvg}
                    isCartBtn={isCartBtn}
                />)}
            </div>
        </div>
    )
}

ItemList.defaultProps = {
    className: "item-wrapper",
}

export default ItemList;