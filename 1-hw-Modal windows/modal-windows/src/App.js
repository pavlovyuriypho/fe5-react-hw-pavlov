import Button from "./Components/Buttons/Button";
import Modal from "./Components/Modal/Modal";
import React, { useState } from "react";
import './App.scss';

const App = () => {
    const [modalActive, setModalActive] = useState(false);
    let handleButtonClick = () => {
        setModalActive(true)
    }

    const [modalTwoActive, setModalTwoActive] = useState(false);
    let handleButtonTwoClick = () => {
        setModalTwoActive(true)
    }

    return (
        <div>
            <div className='btnWr'>
                <Button bgColor='rgba(0, 53, 186, 1)' Click = {() => handleButtonClick()} text = { 'Open first modal' }>
                </Button>
                <Button bgColor='rgba(200, 33, 186, 1)' Click = {() => handleButtonTwoClick()} text = { 'Open second modal' }/>
            </div>

            <Modal actions={
                <>
                    <button className='modal-btn'>Ok</button>
                    <button className='modal-btn'>Cancel</button>
                </>
            }
                   header='Do you want to delete this file?'
                closeButton = { true }
                   modalText='Once you delete this file, it won’t be possible to undo this action.
            Are you sure you want to delete it?' active={modalActive} setActive={setModalActive}/>

            <Modal actions={
                <>
                    <button className='modal-btn'>Ok</button>
                    <button className='modal-btn'>Cancel</button>
                </>
            }
                    header='Hello, Word!'
               closeButton = { true }
                   modalText='Lorem ipsum, dolor sit amet consectetur adipisicing elit. Impedit, ratione nihil voluptate sed consequuntur nostrum, mollitia eaque, nobis nisi eos corrupti autem minima ab possimus deleniti ducimus saepe esse animi.' active={modalTwoActive} setActive={setModalTwoActive}/>

        </div>);
}

export default App;