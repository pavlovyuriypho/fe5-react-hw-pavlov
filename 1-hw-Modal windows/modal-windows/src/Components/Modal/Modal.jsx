import React from "react";

const Modal = ({active, closeButton, actions, setActive, header, modalText}) => {

    return (
        <div className={active ? 'modal active' : 'modal'} onClick={() => setActive(false)}>
            <div className = { active ? 'modal-content active' : 'modal-content' } onClick={e => e.stopPropagation()}>
                <h1 className='modal-title'>
                    { header }
                </h1>

                { closeButton && <button className='close'></button> }

                <p className = 'modal-text'>
                    { modalText }
                </p>
                <div className='modal-btn-wr'>
                    { actions && actions }
                </div>
            </div>
        </div>
    )
}

export default Modal;