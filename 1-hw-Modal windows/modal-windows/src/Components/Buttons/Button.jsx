import {Component} from "react";

class Button extends Component {
    render() {
        const { bgColor, Click, text } = this.props
        return (
                <button className={'btn'} style={{background: bgColor}} onClick = { Click }>{ text }</button>
            )
    }
}

export default Button;