import {createSlice} from "@reduxjs/toolkit";
import getData from "../itemRequest/getData";

const initialState = {
    items: [],
    isModal: false,
};

const itemsSlice = createSlice({
    name: "items",
    initialState,
    reducers: {
        actionSetItems: (state, action) => {
            state.items = [...action.payload];
        },
        actionHandleModal: (state, action) => {
            state.isModal = action.payload;
        }
    }
});

export const {actionSetItems, actionHandleModal} = itemsSlice.actions;
export const actionFetchItems = () => (dispatch) => {
    return getData()
        .then(data => {
            dispatch(actionSetItems(data));
        })
}

export const actionSetModal = (isModal) => (dispatch) => {
    return dispatch(actionHandleModal(!isModal));
}

export default itemsSlice.reducer;